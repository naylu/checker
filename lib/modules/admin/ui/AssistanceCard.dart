import 'package:checker/modules/admin/data/models/Assistance.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AssistanceCard extends StatelessWidget {
  AssistanceCard({this.employeeInformation});

  final Assistance employeeInformation;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Text(employeeInformation.fullName+" ("+employeeInformation.role+")" ,style:TextStyle(fontWeight: FontWeight.w700)),
            SizedBox(height: 16,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Text("Entrada",style:TextStyle(fontWeight: FontWeight.w700)),
                    SizedBox(height: 8,),
                    Text(employeeInformation.startWork, style: TextStyle(color: employeeInformation.startWorkColor),),
                    SizedBox(height: 8,),
                    Text(employeeInformation.startLunch, style: TextStyle(color: employeeInformation.startLunchColor),)
                  ],
                ),
                SizedBox(width: 20,),
                Column(

                  children: [
                    Text("Salida",style:TextStyle(fontWeight: FontWeight.w700)),
                    SizedBox(height: 8,),
                    Text(employeeInformation.endWork, style: TextStyle(color: employeeInformation.endWorkColor),),
                    SizedBox(height: 8,),
                    Text(employeeInformation.endLunch, style: TextStyle(color: employeeInformation.endLunchColor),)
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
