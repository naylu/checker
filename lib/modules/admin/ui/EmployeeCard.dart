import 'package:checker/common/AppTheme.dart';
import 'package:checker/modules/admin/data/models/EmployeeListResponse.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmployeeCard extends StatefulWidget {
  EmployeeCard(this.employeeInfo);

  final EmployeeListResponse employeeInfo;
  @override
  _EmployeeCardState createState() => _EmployeeCardState();
}

class _EmployeeCardState extends State<EmployeeCard> {
  String rol;

  String getEnumRole(String role) {
    if (role == 'boss') return 'Jefe';
    if (role == 'employee') return 'Colaborador';
    if (role == 'employeeHalf') return 'Medio tiempo';
    return 'Gerente';
  }


  @override
  void initState() {
    super.initState();
    setState(() {
      rol = getEnumRole(widget.employeeInfo.role);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Icon(Icons.person_pin, size: 36,),
              SizedBox(width: 16,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.employeeInfo.firstName+" "+ widget.employeeInfo.lastName, style: TextStyle(fontWeight: FontWeight.w500, fontSize: 24),),
                  Text(widget.employeeInfo.id.toString()+" - "+ rol, style: TextStyle(fontSize: 20),)
                ],
              ),
            ],
          ),
          Divider(color: AppColors.grayMainColor,)
        ],
      ),
    );
  }
}

