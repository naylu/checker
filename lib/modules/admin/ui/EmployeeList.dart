import 'package:checker/common/Alert.dart';
import 'package:checker/common/AppTheme.dart';
import 'package:checker/modules/admin/data/AdminRepository.dart';
import 'package:checker/modules/admin/data/models/EmployeeListResponse.dart';
import 'package:checker/modules/admin/ui/EmployeeCard.dart';
import 'package:checker/modules/admin/ui/RegisterEmployee.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmployeeList extends StatefulWidget {
  @override
  _EmployeeListState createState() => _EmployeeListState();
}

class _EmployeeListState extends State<EmployeeList> {
  final adminRepository = AdminRepository();
  List<EmployeeListResponse> employeeList=[];
  bool loading = true;

  @override
  void initState() {
    super.initState();
    getEmployee();
  }

  getEmployee() async {
    try{
      final res = await adminRepository.getEmployee();
      setState(() {
        employeeList = res;
        loading = false;
      });
    }catch(error){
      setState(() {
        loading = false;
      });
      Alert.show(context, message: "No se ha podido obtener a los colaboradores");
    }
  }

  @override
  Widget build(BuildContext context) {
    if(loading){
      return Container(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    return Container(
      child: Stack(
        children: [
          SingleChildScrollView(
            child: Column(children:
              employeeList.map((e) => EmployeeCard(e)).toList(),),
          ),
          Positioned(
            bottom: 16,
            right: 16,
            child: RawMaterialButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RegisterEmployee(refreshList:getEmployee())));
              },
              elevation: 2.0,
              fillColor: AppColors.secondaryColor,
              child: Icon(
                Icons.add,
                size: 35.0,
                color: Colors.white,
              ),
              padding: EdgeInsets.all(15.0),
              shape: CircleBorder(),
            ),)
        ],
      ),
    );
  }
}
