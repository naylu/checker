import 'package:checker/common/AppTheme.dart';
import 'package:checker/modules/admin/data/AdminRepository.dart';
import 'package:checker/modules/admin/ui/AssistanceList.dart';
import 'package:checker/modules/admin/ui/LoginPage.dart';
import 'file:///C:/Users/Naylu/Documents/checker/lib/modules/admin/ui/EmployeeList.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainAdmin extends StatefulWidget {
  @override
  _MainAdminState createState() => _MainAdminState();
}

class _MainAdminState extends State<MainAdmin> {
  final adminRepository = AdminRepository();
  String fullName;

  @override
  void initState() {
    super.initState();
    getName();
  }

  getName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      fullName = prefs.getString(FULLNAME);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      // ignore: missing_return
      onWillPop: () async {
        await adminRepository.logout();
        Navigator.pushAndRemoveUntil(context,
            MaterialPageRoute(builder: (context) => LoginPage()),
            ModalRoute.withName('/'));
      },
      child: MaterialApp(
        home: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: AppColors.primaryColor,
              bottom: TabBar(
                tabs: [
                  Tab(text: "Colaboradores",),
                  Tab(text: "Asistencia"),
                ],
              ),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(fullName ?? "", overflow: TextOverflow.ellipsis),
                  IconButton(
                      icon: Icon(Icons.logout),
                      onPressed: () async {
                    await adminRepository.logout();
                    Navigator.pushAndRemoveUntil(context,
                        MaterialPageRoute(builder: (context) => LoginPage()),
                        ModalRoute.withName('/'));
                  })
                ],
              ),
            ),
            body: TabBarView(
              children: [
                EmployeeList(),
                AssistanceList(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
