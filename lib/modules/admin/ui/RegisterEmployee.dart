import 'package:checker/common/Alert.dart';
import 'package:checker/common/AppTheme.dart';
import 'package:checker/common/ButtonPrimary.dart';
import 'package:checker/common/TextFieldInput.dart';
import 'package:checker/common/Validations.dart';
import 'package:checker/modules/admin/data/AdminRepository.dart';
import 'package:checker/modules/admin/data/models/RoleListResponse.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';

class RegisterEmployee extends StatefulWidget {
  RegisterEmployee({this.refreshList});
  final Future refreshList;

  @override
  _RegisterEmployeeState createState() => _RegisterEmployeeState();
}

class _RegisterEmployeeState extends State<RegisterEmployee> {
  final formKey = new GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _lastnameController = TextEditingController();
  final _mLastnameController = TextEditingController();
  final _phoneController = TextEditingController();
  final _checkCodeController = TextEditingController();
  String role = 'Gerente';
  String hour = "09:00";
  String hourFinish = "13:00";
  bool buttonEnabled = false;
  bool nameValid = false;
  bool lastnameValid = false;
  bool mLastNameValid = true;
  bool phoneValid = false;
  bool checkCodeValid = false;
  bool loading = false;

  Map<String, FocusNode> _focusNodes = {
    "name": FocusNode(),
    "lastName": FocusNode(),
    "mLastname": FocusNode(),
    "phone": FocusNode(),
    "checkCode": FocusNode(),
    "startWork": FocusNode(),
  };

  _updateButtonState() {
    setState(() {
      buttonEnabled = nameValid &&
          lastnameValid &&
          mLastNameValid &&
          phoneValid &&
          checkCodeValid &&
          loading == false;
    });
  }

  _onSubmit() async {
    final adminRepository = AdminRepository();
    String enumRole;
    String startWork;
    String endWork;
    String startLunch;
    String endLunch;
    if (loading == true) {
      return;
    }
    try {
      final form = formKey.currentState;
      if (form.validate()) {
        setState(() {
          loading = true;
          _updateButtonState();
        });
        enumRole = getEnumRole();
        print(enumRole);
        if (enumRole == 'employeeHalf') {
          startWork = hour;
          endWork = hourFinish;
        } else {
          List<RoleListResponse> roleList = await adminRepository.getRole();
          startWork = roleList
              .firstWhere((element) => element.role == enumRole)
              .startWork;
          endWork = roleList
              .firstWhere((element) => element.role == enumRole)
              .endWork;
          startLunch = roleList
              .firstWhere((element) => element.role == enumRole)
              .startLunch;
          endLunch = roleList
              .firstWhere((element) => element.role == enumRole)
              .endLunch;
        }
        final resp = await adminRepository.registerEmployee(
            firstName: _nameController.text,
            lastName: _lastnameController.text,
            mLastName: _mLastnameController.text,
            checkCode: _checkCodeController.text,
            phone: _phoneController.text,
            role: enumRole,
            startWork: startWork,
            endWork: endWork,
            startLunch: startLunch,
            endLunch: endLunch);
        showDialogMessage(
            context: context, id: resp.id.toString(), checkCode: _checkCodeController.text);
        clear();
        _updateButtonState();
        setState(() {
          loading = false;
        });
      }
    } catch (error) {
      setState(() {
        loading = false;
      });
      _updateButtonState();
      Alert.show(context,
          message:
              "No se ha podido iniciar sesión. Intente de nuevo más tarde");
    }
  }

  String getEnumRole() {
    if (role == 'Jefe') return 'boss';
    if (role == 'Colaborador') return 'employee';
    if (role == 'Medio tiempo') return 'employeeHalf';
    return 'manager';
  }

  clear() {
    _nameController.clear();
    _lastnameController.clear();
    _mLastnameController.clear();
    _checkCodeController.clear();
    _phoneController.clear();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return WillPopScope(
      onWillPop: (){
        // ignore: missing_return
        widget.refreshList;
        Navigator.pop(context);
      },
      child: Scaffold(
        backgroundColor: AppColors.background,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(right: 32, bottom: 16, top: 16),
              height: mediaQuery.size.height,
              child: Stack(
                children: [
                  Column(
                    children: [
                      Text(
                        "Registro",
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(
                        height: 32,
                      ),
                      Form(
                        key: formKey,
                        autovalidateMode: AutovalidateMode.always,
                        child: Column(
                          children: [
                            TextFieldInput(
                              labelText: "Nombre",
                              controller: _nameController,
                              focusNode: _focusNodes['name'],
                              textInputAction: TextInputAction.next,
                              color: AppColors.grayMainColor,
                              onSubmitted: (value) {
                                FocusScope.of(context)
                                    .requestFocus(_focusNodes['lastname']);
                              },
                              validator: (val) {
                                if (val.isEmpty) {
                                  setState(() {
                                    nameValid = false;
                                  });
                                  return "El nombre es requerido";
                                }
                                if (validateOnlyLetters(val) != null) {
                                  setState(() {
                                    nameValid = false;
                                  });
                                  return "El nombre es incorrecto";
                                } else {
                                  setState(() {
                                    nameValid = true;
                                  });
                                }
                                return null;
                              },
                              onChanged: (value) {
                                _updateButtonState();
                              },
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            TextFieldInput(
                              labelText: "Apellido paterno",
                              controller: _lastnameController,
                              focusNode: _focusNodes['lastname'],
                              textInputAction: TextInputAction.next,
                              color: AppColors.grayMainColor,
                              onSubmitted: (value) {
                                FocusScope.of(context)
                                    .requestFocus(_focusNodes['mLastname']);
                              },
                              validator: (val) {
                                if (val.isEmpty) {
                                  setState(() {
                                    lastnameValid = false;
                                  });
                                  if (validateOnlyLetters(val) != null) {
                                    setState(() {
                                      lastnameValid = false;
                                    });
                                    return "El apellido es incorrecto";
                                  }
                                  return "El apellido paterno es requerido";
                                } else {
                                  setState(() {
                                    lastnameValid = true;
                                  });
                                }
                                return null;
                              },
                              onChanged: (value) {
                                _updateButtonState();
                              },
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            TextFieldInput(
                              labelText: "Apellido materno",
                              controller: _mLastnameController,
                              focusNode: _focusNodes['mLastname'],
                              textInputAction: TextInputAction.next,
                              color: AppColors.grayMainColor,
                              onSubmitted: (value) {
                                FocusScope.of(context)
                                    .requestFocus(_focusNodes['phone']);
                              },
                              validator: (val) {
                                if (val.isNotEmpty) {
                                  if (validateOnlyLetters(val) != null) {
                                    setState(() {
                                      mLastNameValid = false;
                                    });
                                    return "El apellido materno es incorrecto";
                                  }
                                }
                                setState(() {
                                  mLastNameValid = true;
                                });
                                return null;
                              },
                              onChanged: (value) {
                                _updateButtonState();
                              },
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            TextFieldInput(
                              labelText: "Código",
                              controller: _checkCodeController,
                              isPassword: true,
                              color: AppColors.grayMainColor,
                              textInputAction: TextInputAction.next,
                              focusNode: _focusNodes['checkCode'],
                              onSubmitted: (value) {
                                FocusScope.of(context)
                                    .requestFocus(_focusNodes['phone']);
                              },
                              validator: (val) {
                                if (val.isEmpty) {
                                  setState(() {
                                    checkCodeValid = false;
                                  });
                                  return "Clave es requerida";
                                } else {
                                  setState(() {
                                    checkCodeValid = true;
                                  });
                                }
                                return null;
                              },
                              onChanged: (value) {
                                _updateButtonState();
                              },
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            TextFieldInput(
                              labelText: "Teléfono",
                              maxLength: 10,
                              controller: _phoneController,
                              focusNode: _focusNodes['phone'],
                              keyboardType: TextInputType.number,
                              color: AppColors.grayMainColor,
                              onSubmitted: (value) {
                                _focusNodes['phone'].unfocus();
                              },
                              validator: (val) {
                                if (val.isEmpty) {
                                  setState(() {
                                    phoneValid = false;
                                  });
                                  return "El teléfono es requerido";
                                }
                                if (val.length < 10) {
                                  setState(() {
                                    phoneValid = false;
                                  });
                                  return "El teléfono debe tener 10 dígitos";
                                }
                                if (validateOnlyNumber(val) != null) {
                                  setState(() {
                                    phoneValid = false;
                                  });
                                  return "El teléfono es incorrecto";
                                } else {
                                  setState(() {
                                    phoneValid = true;
                                  });
                                }
                                return null;
                              },
                              onChanged: (value) {
                                _updateButtonState();
                              },
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 32),
                                  child: DropdownButton<String>(
                                    hint: Text(
                                      role,
                                      style: TextStyle(
                                          color: AppColors.grayMainColor),
                                    ),
                                    items: <String>[
                                      'Gerente',
                                      'Jefe',
                                      'Colaborador',
                                      'Medio tiempo'
                                    ].map((String value) {
                                      return new DropdownMenuItem<String>(
                                        value: value,
                                        child: new Text(
                                          value,
                                          style: TextStyle(
                                              color: AppColors.grayMainColor),
                                        ),
                                      );
                                    }).toList(),
                                    onChanged: (val) {
                                      setState(() {
                                        role = val;
                                      });
                                    },
                                  ),
                                ),
                                role == 'Medio tiempo'
                                    ? Row(
                                        children: [
                                          Text("Entrada:"),
                                          FlatButton(
                                              onPressed: () {
                                                DatePicker.showTimePicker(context,
                                                    onConfirm: (date) {
                                                  setState(() {
                                                    hour = DateFormat('HH:mm')
                                                        .format(date);
                                                    hourFinish = DateFormat(
                                                            'HH:mm')
                                                        .format(date.add(
                                                            Duration(hours: 4)));
                                                  });
                                                },
                                                    currentTime: DateTime.now(),
                                                    locale: LocaleType.es);
                                              },
                                              child: Text(
                                                hour,
                                                style: TextStyle(
                                                    color:
                                                        AppColors.grayMainColor),
                                              )),
                                        ],
                                      )
                                    : SizedBox(),
                              ],
                            ),
                            SizedBox(
                              height: 32,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left:32),
                              child: ButtonPrimary(
                                  onPressed: _onSubmit,
                                  text: "Registrar",
                                  enable: buttonEnabled),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                      top: 0,
                      left: 0,
                      child: IconButton(
                        icon: Icon(
                          Icons.arrow_back_ios,
                          color: AppColors.grayMainColor,
                        ),
                        onPressed: () {
                          widget.refreshList;
                          Navigator.pop(context);
                        },
                      ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
