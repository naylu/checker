import 'package:checker/common/Alert.dart';
import 'package:checker/common/AppTheme.dart';
import 'package:checker/modules/admin/data/AdminRepository.dart';
import 'package:checker/modules/admin/data/models/Assistance.dart';
import 'package:checker/modules/admin/data/models/CheckerListResponse.dart';
import 'package:checker/modules/admin/data/models/EmployeeListResponse.dart';
import 'package:checker/modules/admin/ui/AssistanceCard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AssistanceList extends StatefulWidget {
  @override
  _AssistanceListState createState() => _AssistanceListState();
}

class _AssistanceListState extends State<AssistanceList> {
  final adminRepository = AdminRepository();
  List<EmployeeListResponse> employeeList = [];
  List<Assistance> assistanceList = [];
  String date = "";
  bool loading = true;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null){
      setState(() {
        date = DateFormat('yyyy-MM-dd').format(pickedDate);
      });
      await getChecker();
    }
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      date = DateFormat('yyyy-MM-dd').format(DateTime.now());
    });
    getEmployeeList();
  }

  getEmployeeList() async {
    try {
      employeeList = await adminRepository.getEmployee();
      getChecker();
    } catch (error) {
      setState(() {
        loading = false;
      });
      Alert.show(context,
          message: "No se ha podido obtener a los colaboradores");
    }
  }

  getChecker() async {
    setState(() {
      loading=true;
    });
    assistanceList.clear();
    for (int i = 0; i < employeeList.length; i++) {
      final employee = employeeList.elementAt(i);
      String startWork = "";
      String startLunch = "";
      String endWork = "";
      String endLunch = "";
      Color startWorkColor;
      Color startLunchColor;
      Color endWorkColor;
      Color endLunchColor;
      List<CheckerListResponse> startWorkList = [];
      List<CheckerListResponse> startLunchList = [];
      List<CheckerListResponse> endWorkList = [];
      List<CheckerListResponse> endLunchList = [];
      try {
        final employeeInformation =
            await adminRepository.getEmployeeById(employee.id);
        List<CheckerListResponse> checkerListResponse =
            await adminRepository.getCheckerList(employee.id, date);
        startWorkList = checkerListResponse
            .where((element) =>
                DateFormat.Hm().parse(element.hour).isAfter(DateFormat.Hm()
                    .parse(employeeInformation.startWork)
                    .subtract(Duration(hours: 1))) &&
                DateFormat.Hm().parse(element.hour).isBefore(DateFormat.Hm()
                    .parse(employeeInformation.startWork)
                    .add(Duration(hours: 1))))
            .toList();
        endWorkList = checkerListResponse
            .where((element) =>
                DateFormat.Hm().parse(element.hour).isAfter(DateFormat.Hm()
                    .parse(employeeInformation.endWork)
                    .subtract(Duration(hours: 1))) &&
                DateFormat.Hm().parse(element.hour).isBefore(DateFormat.Hm()
                    .parse(employeeInformation.endWork)
                    .add(Duration(hours: 1))))
            .toList();
        startWork =
            startWorkList.isNotEmpty ? startWorkList.elementAt(0).hour : "";
        endWork = endWorkList.isNotEmpty ? endWorkList.elementAt(0).hour : "";
        if (startWork != "") {
          if (DateFormat.Hm().parse(startWork).isAfter(DateFormat.Hm()
                  .parse(employeeInformation.startWork)
                  .subtract(Duration(minutes: 15))) &&
              DateFormat.Hm().parse(startWork).isBefore(DateFormat.Hm()
                  .parse(employeeInformation.startWork)
                  .add(Duration(minutes: 15)))) {
            startWorkColor = Colors.green;
          } else {
            startWorkColor = Colors.red;
          }
        }

        if (endWork != "") {
          if (DateFormat.Hm().parse(endWork).isAfter(DateFormat.Hm()
                  .parse(employeeInformation.endWork)
                  .subtract(Duration(minutes: 15))) &&
              DateFormat.Hm().parse(endWork).isBefore(DateFormat.Hm()
                  .parse(employeeInformation.endWork)
                  .add(Duration(minutes: 15)))) {
            endWorkColor = Colors.green;
          } else {
            endWorkColor = Colors.red;
          }
        }

        if (employeeInformation.role != "employeeHalf") {
          startLunchList = checkerListResponse
              .where((element) =>
                  DateFormat.Hm().parse(element.hour).isAfter(DateFormat.Hm()
                      .parse(employeeInformation.startLunch)
                      .subtract(Duration(hours: 1))) &&
                  DateFormat.Hm().parse(element.hour).isBefore(DateFormat.Hm()
                      .parse(employeeInformation.startLunch)
                      .add(Duration(hours: 1))))
              .toList();
          endLunchList = checkerListResponse
              .where((element) =>
                  DateFormat.Hm().parse(element.hour).isAfter(DateFormat.Hm()
                      .parse(employeeInformation.endLunch)
                      .subtract(Duration(hours: 1))) &&
                  DateFormat.Hm().parse(element.hour).isBefore(DateFormat.Hm()
                      .parse(employeeInformation.endLunch)
                      .add(Duration(hours: 1))))
              .toList();
          startLunch =
              startLunchList.isNotEmpty ? startLunchList.elementAt(0).hour : "";
          endLunch =
              endLunchList.isNotEmpty ? endLunchList.elementAt(0).hour : "";
          if (startLunch != "") {
            if (DateFormat.Hm().parse(startLunch).isAfter(DateFormat.Hm()
                    .parse(employeeInformation.startLunch)
                    .subtract(Duration(minutes: 15))) &&
                DateFormat.Hm().parse(startLunch).isBefore(DateFormat.Hm()
                    .parse(employeeInformation.startLunch)
                    .add(Duration(minutes: 15)))) {
              startLunchColor = Colors.green;
            } else {
              startLunchColor = Colors.red;
            }
          }
          if (endLunch != "") {
            if (DateFormat.Hm().parse(endLunch).isAfter(DateFormat.Hm()
                    .parse(employeeInformation.endLunch)
                    .subtract(Duration(minutes: 15))) &&
                DateFormat.Hm().parse(endLunch).isBefore(DateFormat.Hm()
                    .parse(employeeInformation.endLunch)
                    .add(Duration(minutes: 15)))) {
              endLunchColor = Colors.green;
            } else {
              endLunchColor = Colors.red;
            }
          }
        }
      } catch (_) {}
      String role = getEnumRole(employee.role);
      setState(() {
        assistanceList.add(Assistance(
            fullName: employee.firstName + " " + employee.lastName + " "+ employee.mLastName,
            role: role,
            startWork: startWork,
            startWorkColor: startWorkColor,
            endWork: endWork,
            endWorkColor: endWorkColor,
            startLunch: startLunch,
            startLunchColor: startLunchColor,
            endLunch: endLunch,
            endLunchColor: endLunchColor));
      });
    }
    setState(() {
      loading= false;
    });
  }
  String getEnumRole(String role) {
    if (role == 'boss') return 'Jefe';
    if (role == 'employee') return 'Colaborador';
    if (role == 'employeeHalf') return 'Medio tiempo';
    return 'Gerente';
  }

  @override
  Widget build(BuildContext context) {

    if (loading) {
      return Container(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }

    return Container(
        padding: EdgeInsets.all(16),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Asistencia del :  "),
                        RaisedButton(
                          onPressed: () => _selectDate(context),
                          child: Text(date),
                        ),
                      ],
                    ),
                    Divider(
                      color: AppColors.grayMainColor,
                    )
                  ],
                ),
              ),
              Column(
                children:
                assistanceList.map((e) => AssistanceCard(employeeInformation: e,)).toList(),
              )
            ],
          ),
        ));
  }
}
