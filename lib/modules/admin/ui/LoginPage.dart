import 'package:checker/common/Alert.dart';
import 'package:checker/common/AppTheme.dart';
import 'package:checker/common/ButtonPrimary.dart';
import 'package:checker/common/TextFieldInput.dart';
import 'package:checker/modules/admin/data/AdminRepository.dart';
import 'package:checker/modules/admin/ui/MainPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final formKey = new GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  bool usernameValid = false;
  bool passwordValid = false;
  bool buttonEnabled = false;
  bool loading = false;
  Map<String, FocusNode> _focusNodes = {
    "username": FocusNode(),
    "password": FocusNode()
  };

  _updateButtonState() {
    setState(() {
      buttonEnabled = usernameValid && passwordValid && loading == false;
    });
  }

  clear(){
    _usernameController.clear();
    _passwordController.clear();
  }


  _onSubmit() async {
    final adminRepository = AdminRepository();
    if (loading == true) {
      return;
    }
    try {
      final form = formKey.currentState;
      if (form.validate()) {
        setState(() {
          loading = true;
          _updateButtonState();
        });
        await adminRepository.login(_usernameController.text, _passwordController.text);
        clear();
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => MainAdmin()));
        _updateButtonState();
      }
    } catch (error) {
      setState(() {
        loading = false;
      });
      _updateButtonState();
      Alert.show(context, message: "No se ha podido iniciar sesión. Intente de nuevo más tarde");
    }
  }



  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      backgroundColor: AppColors.background,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(16),
            height: mediaQuery.size.height,
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Inicio de sesión", style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700, ),),
                    SizedBox(height: 64,),
                    Form(
                      key:formKey,
                      child: Column(
                        children: [
                          TextFieldInput(
                            labelText: "Usuario",
                            controller: _usernameController,
                            prefixIcon: Icons.person_outline_sharp,
                            focusNode: _focusNodes['username'],
                            textInputAction: TextInputAction.next,
                            color: AppColors.grayMainColor,
                            onSubmitted: (value) {
                              FocusScope.of(context)
                                  .requestFocus(_focusNodes['password']);
                            },
                            validator: (val) {
                              if (val.isEmpty) {
                                setState(() {
                                  usernameValid = false;
                                });
                                return "Número de registro es requerido";
                              }else{
                                setState(() {
                                  usernameValid = true;
                                });
                              }
                              return null;
                            },
                            onChanged: (value) {
                              _updateButtonState();
                            },
                          ),
                          SizedBox(height: 8,),
                          TextFieldInput(
                            labelText: "Clave",
                            controller: _passwordController,
                            prefixIcon: Icons.lock,
                            isPassword: true,
                            color: AppColors.grayMainColor,
                            focusNode: _focusNodes['password'],
                            onSubmitted: (value) {
                              _focusNodes['password'].unfocus();
                            },
                            validator: (val) {
                              if (val.isEmpty) {
                                setState(() {
                                  passwordValid = false;
                                });
                                return "Clave es requerida";
                              }else{
                                setState(() {
                                  passwordValid = true;
                                });
                              }
                              return null;
                            },
                            onChanged: (value) {
                              _updateButtonState();
                            },

                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 64,),
                    ButtonPrimary(onPressed: _onSubmit, text: "Iniciar sesión", enable: buttonEnabled)
                  ],
                ),
                Positioned(
                    top:0,
                    left:0,
                    child:IconButton(
                      icon:Icon( Icons.arrow_back_ios, color: AppColors.grayMainColor,),
                      onPressed: (){
                        Navigator.pop(context);
                      },
                    ) )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
