import 'package:checker/core/data/ApiService.dart';
import 'package:checker/modules/admin/data/RegisterEmployeeResponse.dart';
import 'package:checker/modules/admin/data/models/CheckerListPayload.dart';
import 'package:checker/modules/admin/data/models/CheckerListResponse.dart';
import 'package:checker/modules/admin/data/models/EmployeeListResponse.dart';
import 'package:checker/modules/admin/data/models/EmployeeResponse.dart';
import 'package:checker/modules/admin/data/models/LoginPayload.dart';
import 'package:checker/modules/admin/data/models/LoginResponse.dart';
import 'package:checker/modules/admin/data/models/RegisterEmployeePayload.dart';
import 'package:checker/modules/admin/data/models/RoleListResponse.dart';

class AdminService {
  static Future <LoginResponse> login(LoginPayload payload) async {
    final result = await ApiService.post("auth/login", payload.toJson());
    return LoginResponse.fromJson(result.data);
  }

  static Future<List<EmployeeListResponse>> getEmployeeList() async {
    final result = await ApiService.getList("employee");
    return (result.data as List).map((x)=>EmployeeListResponse.fromJson(x)).toList();
  }

  static Future <RegisterEmployeeResponse> registerEmployee(RegisterEmployeePayload payload) async {
    final result = await ApiService.post("employee", payload.toJson());
    return RegisterEmployeeResponse.fromJson(result.data);
  }

  static Future<List<RoleListResponse>> getRoleList() async {
    final result = await ApiService.getList("roleSchedule");
    return (result.data as List).map((x)=>RoleListResponse.fromJson(x)).toList();
  }

  static Future<EmployeeResponse> getEmployee(int id) async {
    final result = await ApiService.get("employee/$id");
    return EmployeeResponse.fromJson(result.data);
  }

  static Future<List<CheckerListResponse>> getCheckerList(CheckerListPayload payload) async {
    final result = await ApiService.postList("checker/list", payload.toJson());
    return (result.data as List).map((x)=>CheckerListResponse.fromJson(x)).toList();
  }
}
