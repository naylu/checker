import 'package:checker/modules/admin/data/AdminService.dart';
import 'package:checker/modules/admin/data/RegisterEmployeeResponse.dart';
import 'package:checker/modules/admin/data/models/CheckerListPayload.dart';
import 'package:checker/modules/admin/data/models/CheckerListResponse.dart';
import 'package:checker/modules/admin/data/models/EmployeeListResponse.dart';
import 'package:checker/modules/admin/data/models/EmployeeResponse.dart';
import 'package:checker/modules/admin/data/models/LoginPayload.dart';
import 'package:checker/modules/admin/data/models/LoginResponse.dart';
import 'package:checker/modules/admin/data/models/RegisterEmployeePayload.dart';
import 'package:checker/modules/admin/data/models/RoleListResponse.dart';
import 'package:shared_preferences/shared_preferences.dart';

const TOKEN = 'token';
const ID = 'id';
const FULLNAME = 'fullName';
const PROFILE = 'profile';

class AdminRepository {
  static final AdminRepository _singleton = new AdminRepository._internal();

  factory AdminRepository() {
    return _singleton;
  }

  AdminRepository._internal();

  String _token;
  String _id;
  String _fullName;
  String _profile;

  Future<bool> get isAuthenticated async => await token != null;

  Future<String> get token async {
    if (_token != null) {
      return _token;
    }
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(TOKEN);
  }

  Future<String> get id async {
    if (await token == null) {
      return null;
    }

    if (_id != null) {
      return _id;
    }

    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(ID);
  }

  Future<String> get fullName async {
    if (await token == null) {
      return null;
    }

    if (_fullName != null) {
      return _fullName;
    }

    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(FULLNAME);
  }

  Future<String> get profile async {
    if (await token == null) {
      return null;
    }

    if (_profile != null) {
      return _profile;
    }

    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(PROFILE);
  }

  Future<void> _setAuthInfo(
      {String id, String token, String profile, String fullName}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _id = id;
    _token = token;
    _fullName = fullName;
    _profile = profile;
    prefs.setString(ID, _id);
    prefs.setString(TOKEN, _token);
    prefs.setString(FULLNAME, _fullName);
    prefs.setString(PROFILE, _profile);
  }

  Future<void> _removeAuthInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _id = null;
    _token = null;
    _fullName = null;
    _profile = null;
    prefs.remove(ID);
    prefs.remove(TOKEN);
    prefs.remove(PROFILE);
    prefs.remove(FULLNAME);
  }

  Future<void> logout() async {
    await _removeAuthInfo();
  }

  Future<LoginResponse> login(String userName, String password) async {
    final response = await AdminService.login(LoginPayload(
      username: userName,
      password: password,
    ));
    await _setAuthInfo(
        id: response.user.id.toString(),
        token: response.token,
        profile: "Administrador",
        fullName: response.user.firstName + " " + response.user.lastName);
    return response;
  }

  Future<List<EmployeeListResponse>> getEmployee() async {
    return AdminService.getEmployeeList();
  }

  Future<RegisterEmployeeResponse> registerEmployee(
      {String firstName,
      String lastName,
      String mLastName,
      String phone,
      String checkCode,
      String role,
      String startWork,
      String endWork,
      String startLunch,
      String endLunch}) async {
    return AdminService.registerEmployee(RegisterEmployeePayload(
        firstName: firstName,
        lastName: lastName,
        mLastName: mLastName,
        phone: phone,
        checkCode: checkCode,
        role: role,
        startWork: startWork,
        endWork: endWork,
        startLunch: startLunch,
        endLunch: endLunch));
  }

  Future<List<RoleListResponse>> getRole() async {
    return AdminService.getRoleList();
  }

  Future<EmployeeResponse> getEmployeeById(int id) async{
    return AdminService.getEmployee(id);
  }

  Future<List<CheckerListResponse>> getCheckerList(int id, String date) async {
    return AdminService.getCheckerList(CheckerListPayload(employeeId: id, date: date));
  }
  
}
