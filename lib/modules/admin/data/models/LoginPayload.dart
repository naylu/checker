class LoginPayload {
  LoginPayload({
    this.username,
    this.password,
  });

  String username;
  String password;

  factory LoginPayload.fromJson(Map<String, dynamic> json) => LoginPayload(
    username: json["username"],
    password: json["password"],
  );

  Map<String, dynamic> toJson() => {
    "username": username,
    "password": password,
  };
}