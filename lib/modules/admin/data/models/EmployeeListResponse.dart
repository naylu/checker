class EmployeeListResponse {
  EmployeeListResponse({
    this.id,
    this.firstName,
    this.lastName,
    this.mLastName,
    this.phone,
    this.role,
  });

  int id;
  String firstName;
  String lastName;
  String mLastName;
  String phone;
  String role;

  factory EmployeeListResponse.fromJson(Map<String, dynamic> json) => EmployeeListResponse(
    id: json["id"],
    firstName: json["firstName"],
    lastName: json["lastName"],
    mLastName: json["mLastName"],
    phone: json["phone"],
    role: json["role"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "firstName": firstName,
    "lastName": lastName,
    "mLastName": mLastName,
    "phone": phone,
    "role": role,
  };
}

