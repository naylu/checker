class CheckerListResponse {
  CheckerListResponse({
    this.id,
    this.date,
    this.hour,
  });

  int id;
  String date;
  String hour;

  factory CheckerListResponse.fromJson(Map<String, dynamic> json) => CheckerListResponse(
    id: json["id"],
    date: json["date"],
    hour: json["hour"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "date": date,
    "hour": hour,
  };
}
