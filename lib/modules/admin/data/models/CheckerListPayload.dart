class CheckerListPayload {
  CheckerListPayload({
    this.date,
    this.employeeId,
  });

  String date;
  int employeeId;

  factory CheckerListPayload.fromJson(Map<String, dynamic> json) => CheckerListPayload(
    date: json["date"],
    employeeId: json["employeeId"],
  );

  Map<String, dynamic> toJson() => {
    "date": date,
    "employeeId": employeeId,
  };
}
