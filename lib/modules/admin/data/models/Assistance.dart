import 'package:flutter/cupertino.dart';

class Assistance {
  Assistance(
      {this.fullName,
      this.role,
      this.startLunch,
      this.endWork,
      this.startWork,
      this.endLunch,
      this.endLunchColor,
      this.endWorkColor,
      this.startLunchColor,
      this.startWorkColor});

  final String fullName;
  final String role;
  final String startWork;
  final Color startWorkColor;
  final String startLunch;
  final Color startLunchColor;
  final String endWork;
  final Color endWorkColor;
  final String endLunch;
  final Color endLunchColor;
}
