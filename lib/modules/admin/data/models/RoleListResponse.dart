class RoleListResponse {
  RoleListResponse({
    this.id,
    this.role,
    this.startWork,
    this.endWork,
    this.startLunch,
    this.endLunch,
  });

  int id;
  String role;
  String startWork;
  String endWork;
  String startLunch;
  String endLunch;

  factory RoleListResponse.fromJson(Map<String, dynamic> json) => RoleListResponse(
    id: json["id"],
    role: json["role"],
    startWork: json["startWork"],
    endWork: json["endWork"],
    startLunch: json["startLunch"],
    endLunch: json["endLunch"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "role": role,
    "startWork": startWork,
    "endWork": endWork,
    "startLunch": startLunch,
    "endLunch": endLunch,
  };
}

