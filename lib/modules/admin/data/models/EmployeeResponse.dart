class EmployeeResponse {
  EmployeeResponse({
    this.id,
    this.firstName,
    this.lastName,
    this.mLastName,
    this.phone,
    this.checkCode,
    this.role,
    this.startWork,
    this.endWork,
    this.startLunch,
    this.endLunch,
  });

  int id;
  String firstName;
  String lastName;
  String mLastName;
  String phone;
  String checkCode;
  String role;
  String startWork;
  String endWork;
  String startLunch;
  String endLunch;

  factory EmployeeResponse.fromJson(Map<String, dynamic> json) => EmployeeResponse(
    id: json["id"],
    firstName: json["firstName"],
    lastName: json["lastName"],
    mLastName: json["mLastName"],
    phone: json["phone"],
    checkCode: json["checkCode"],
    role: json["role"],
    startWork: json["startWork"],
    endWork: json["endWork"],
    startLunch: json["startLunch"],
    endLunch: json["endLunch"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "firstName": firstName,
    "lastName": lastName,
    "mLastName": mLastName,
    "phone": phone,
    "checkCode": checkCode,
    "role": role,
    "startWork": startWork,
    "endWork": endWork,
    "startLunch": startLunch,
    "endLunch": endLunch,
  };
}
