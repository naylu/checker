class RegisterEmployeeResponse {
  RegisterEmployeeResponse({
    this.id,
  });

  int id;

  factory RegisterEmployeeResponse.fromJson(Map<String, dynamic> json) => RegisterEmployeeResponse(
    id: json["id"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
  };
}