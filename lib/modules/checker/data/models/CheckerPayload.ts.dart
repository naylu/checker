class CheckerPayload {
  CheckerPayload({
    this.employeeId,
    this.date,
    this.hour,
    this.checkCode,
  });

  int employeeId;
  String date;
  String hour;
  String checkCode;

  factory CheckerPayload.fromJson(Map<String, dynamic> json) => CheckerPayload(
    employeeId: json["employeeId"],
    date: json["date"],
    hour: json["hour"],
    checkCode: json["checkCode"],
  );

  Map<String, dynamic> toJson() => {
    "employeeId": employeeId,
    "date": date,
    "hour": hour,
    "checkCode": checkCode,
  };
}
