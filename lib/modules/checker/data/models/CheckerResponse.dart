class CheckerResponse {
  CheckerResponse({
    this.id,
  });

  int id;

  factory CheckerResponse.fromJson(Map<String, dynamic> json) => CheckerResponse(
    id: json["id"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
  };
}