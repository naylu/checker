import 'package:checker/modules/checker/data/CheckerService.dart';
import 'package:checker/modules/checker/data/models/CheckerPayload.ts.dart';
import 'package:checker/modules/checker/data/models/CheckerResponse.dart';

class CheckerRepository {
  static final CheckerRepository _singleton = new CheckerRepository._internal();

  factory CheckerRepository() {
    return _singleton;
  }

  CheckerRepository._internal();


  Future<CheckerResponse> registerChecker(
      {int employeeId, String checkCode, String date, String hour}) async {
    return CheckService.registerChecker(CheckerPayload(checkCode: checkCode, date: date, employeeId: employeeId, hour: hour));
  }
}