import 'package:checker/core/data/ApiService.dart';
import 'package:checker/modules/checker/data/models/CheckerPayload.ts.dart';
import 'package:checker/modules/checker/data/models/CheckerResponse.dart';

class CheckService {
  static Future <CheckerResponse> registerChecker(CheckerPayload payload) async {
    final result = await ApiService.post("checker", payload.toJson());
    return CheckerResponse.fromJson(result.data);
  }
}