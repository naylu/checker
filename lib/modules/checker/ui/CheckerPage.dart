import 'dart:async';

import 'package:checker/common/AppTheme.dart';
import 'package:checker/common/ButtonPrimary.dart';
import 'package:checker/common/TextFieldInput.dart';
import 'package:checker/common/Validations.dart';
import 'package:checker/modules/admin/ui/LoginPage.dart';
import 'package:checker/modules/checker/data/CheckerRepository.dart';
import 'package:checker/modules/checker/ui/ClockTimer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CheckerPage extends StatefulWidget {
  @override
  _CheckerPageState createState() => _CheckerPageState();
}

class _CheckerPageState extends State<CheckerPage> {
  final formKey = new GlobalKey<FormState>();
  final _registerController = TextEditingController();
  final _passwordController = TextEditingController();
  bool registerValid = false;
  bool passwordValid = false;
  bool buttonEnabled = false;
  bool show = false;
  String response = "";

  Map<String, FocusNode> _focusNodes = {
    "register": FocusNode(),
    "password": FocusNode()
  };

  void _updateButtonState() {
    setState(() {
      buttonEnabled = registerValid && passwordValid;
    });
  }

  _onSubmit() async {
    final checkerRepository = CheckerRepository();

    final hour = DateFormat('HH:mm').format(DateTime.now());
    final date = DateFormat('yyyy-MM-dd').format(DateTime.now());

    try{
      await checkerRepository.registerChecker(employeeId: int.parse(_registerController.text), hour: hour, date: date,checkCode: _passwordController.text);
      setState(() {
        response = "Acceso correcto";
      });
      clear();
      _updateButtonState();
    }catch(e,s){
      print(s);
      setState(() {
        response = "Intenté de nuevo";
      });
      _updateButtonState();
    }
    Future.delayed(Duration(seconds:5)).then((value) => setState(() {
      response = "";
    }));
  }

  clear(){
    _registerController.clear();
    _passwordController.clear();
  }


  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      backgroundColor: AppColors.background,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(16),
            height: mediaQuery.size.height,
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ClockTimer(),
                    SizedBox(height: 64,),
                    Text(response, style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700),),
                    SizedBox(height: 64,),
                    Form(
                      key: formKey,
                      autovalidateMode: AutovalidateMode.always,
                      child: Column(
                        children: [
                          TextFieldInput(
                            labelText: "Número de registro",
                            controller: _registerController,
                            prefixIcon: Icons.person_outline_sharp,
                            focusNode: _focusNodes['register'],
                            textInputAction: TextInputAction.next,
                            color: AppColors.grayMainColor,
                            keyboardType: TextInputType.number,
                            onSubmitted: (value) {
                              FocusScope.of(context)
                                  .requestFocus(_focusNodes['password']);
                            },
                            validator: (val) {
                              if (val.isEmpty) {
                                setState(() {
                                  registerValid = false;
                                });
                                return "Número de registro es requerido";
                            }
                            if(validateOnlyNumber(val) != null){
                              setState(() {
                                registerValid = false;
                              });
                              return "Número de registro incorrecto";
                            }
                            else{
                                setState(() {
                                  registerValid = true;
                                });
                              }
                              return null;
                            },
                            onChanged: (value) {
                              _updateButtonState();
                            },
                          ),
                          SizedBox(height: 8,),
                          TextFieldInput(
                            labelText: "Clave",
                            controller: _passwordController,
                            prefixIcon: Icons.lock,
                            color: AppColors.grayMainColor,
                            focusNode: _focusNodes['password'],
                            onSubmitted: (value) {
                              _focusNodes['password'].unfocus();
                            },
                            validator: (val) {
                              if (val.isEmpty) {
                                setState(() {
                                  passwordValid = false;
                                });
                                return "Clave es requerida";
                              }else{
                                setState(() {
                                  passwordValid = true;
                                });
                              }
                              return null;
                            },
                            onChanged: (value) {
                              _updateButtonState();
                            },

                          ),
                          SizedBox(height: 64,),
                          ButtonPrimary(onPressed:_onSubmit, text: "Checar", enable: buttonEnabled)
                        ],
                      ),
                    ),
                  ],
                ),
                Positioned(
                    top:0,
                    right:0,
                    child:IconButton(
                      icon:Icon( Icons.settings),
                      onPressed: (){
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => LoginPage()));
                      },
                    ) )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
