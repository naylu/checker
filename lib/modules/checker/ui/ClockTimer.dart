import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

class ClockTimer extends StatefulWidget {
  @override
  _ClockTimerState createState() => _ClockTimerState();
}

class _ClockTimerState extends State<ClockTimer> {
  String time="";
  Timer timer;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        time= DateFormat('HH : mm : ss').format(DateTime.now());
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(time, style: TextStyle(fontSize: 48, fontWeight: FontWeight.w700),),
    );
  }
}
