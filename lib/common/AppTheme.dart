import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = const Color(0xff21209c);
  static const Color secondaryColor = const Color(0xfffdb827);
  static const Color text = const Color(0xff23120b);
  static const Color background = const Color(0xfff1f1f1);
  static const Color grayColor = const Color(0xff40403F);
  static const Color grayLightColor = const Color(0xffE6E5E5);
  static const Color grayMainColor = const Color(0xffa5a3a3);

}

class AppTheme {
  AppTheme();

  static ThemeData getThemeData() {
    return ThemeData(
      primaryColor: AppColors.primaryColor,
      accentColor: AppColors.secondaryColor,
      disabledColor: AppColors.grayColor,
      buttonColor: AppColors.secondaryColor,
      textTheme: TextTheme(
        bodyText2: TextStyle(fontSize: 16.0),
        headline5: TextStyle(fontSize: 32.0, fontWeight: FontWeight.bold),
        bodyText1: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
      ),
    );
  }
}
