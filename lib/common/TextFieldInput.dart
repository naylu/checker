import 'package:checker/common/AppTheme.dart';
import 'package:flutter/material.dart';

class TextFieldInput extends StatefulWidget {
  TextFieldInput(
      {this.labelText,
        this.color,
        this.prefixIcon,
        this.onChanged,
        this.validator,
        this.focusNode,
        this.keyboardType,
        this.onSubmitted,
        this.controller,
        this.textInputAction,
        this.isPassword = false,
        this.maxLength});

  final String labelText;
  final Color color;
  final IconData prefixIcon;
  final FocusNode focusNode;
  final TextInputType keyboardType;
  final ValueChanged<String> onChanged;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onSubmitted;
  final TextEditingController controller;
  final TextInputAction textInputAction;
  final bool isPassword;
  final int maxLength;

  @override
  _TextFieldInputState createState() => _TextFieldInputState();
}

class _TextFieldInputState extends State<TextFieldInput> {
  bool _isFocused = false;
  FocusNode _node;
  Color fieldColor;
  String text = "";
  bool hidePassword;
  bool hasError = false;
  bool showErrorDetail = false;
  String errorDetail;

  @override
  void initState() {
    _node = widget.focusNode ?? FocusNode();
    _node.addListener(_handleFocusChange);
    super.initState();
    fieldColor = widget.color ?? AppTheme.getThemeData().primaryColor;
    hidePassword = widget.isPassword;
  }

  @override
  Widget build(BuildContext context) {

    return Stack(
      overflow: Overflow.visible,
      children: <Widget>[
        Container(
          height: _isFocused ? 85 : widget.controller.text.isNotEmpty ? 85 : 45,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            textBaseline: TextBaseline.alphabetic,
            children: <Widget>[
              Container(
                height: 20,
                width: 20,
                child: Icon(
                  widget.prefixIcon,
                  color: fieldColor,
                )
              ),
              SizedBox(width: 16,),
              Expanded(
                child: TextFormField(
                  controller: widget.controller,
                  maxLength: widget.maxLength,
                  onChanged: (value) {
                    setState(() {
                      text = value;
                    });
                    if (widget.onChanged != null) {
                      _validateField(value);
                      widget.onChanged(value);
                    }
                  },
                  onFieldSubmitted: widget.onSubmitted,
                  obscureText: this.hidePassword,
                  cursorColor: widget.color,
                  focusNode: _node,
                  validator: (val){
                    return errorDetail;
                  },
                  autocorrect: false,
                  keyboardType: widget.keyboardType,
                  textInputAction: widget.textInputAction,
                  decoration: new InputDecoration(
                    labelText: widget.labelText,
                    focusColor: Colors.white,
                    labelStyle: TextStyle(
                        color: fieldColor,
                        fontSize: 16.0,
                        fontWeight: FontWeight.w400),
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                      BorderSide(color: fieldColor, width: 1.0),
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: fieldColor,
                      ),
                    ),
                  ),
                  style: TextStyle(
                      color: fieldColor,
                      fontSize: 16,
                      fontWeight: FontWeight.w400
                  ),
                ),
              ),
            ],
          ),
        ),
        Positioned(
          right: 0,
          top: _isFocused ? 25:  widget.controller.text.isNotEmpty ? 25 : 10,
          child: SizedBox(
            height: 40.0,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                togglePassword(),
                SizedBox(
                  width: 5,
                ),
              ],
            ),
          ),
        ),
        Positioned(
          top: 0,
          right: 0,
          child: Container(),
        ),
      ],
    );
  }

  String _validateField(String text) {
    String error = widget.validator(text);
    if (error != null) {
      setState(() {
        this.hasError = true;
        this.errorDetail = error;
      });
    } else {
      setState(() {
        this.errorDetail = null;
        this.hasError = false;
      });
      return null;
    }
    return null;
  }

  void _handleFocusChange() async {
    if (_node.hasFocus != _isFocused) {
      setState(() {
        _isFocused = _node.hasFocus;
      });
    }
  }

  Widget togglePassword() {
    if (widget.isPassword) {
      return GestureDetector(
        onTap: () {
          setState(() {
            this.hidePassword = !this.hidePassword && widget.isPassword;
          });
        },
        child: Icon(
          this.hidePassword ? Icons.visibility : Icons.visibility_off,
          color: fieldColor,
        )
      );
    } else {
      return SizedBox(
        width: 0,
      );
    }
  }

  @override
  void dispose() {
    _node.removeListener(_handleFocusChange);
    _node.dispose();
    super.dispose();
  }
}
