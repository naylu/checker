import 'package:checker/common/AppTheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ButtonPrimary extends StatelessWidget {

  ButtonPrimary({@required this.onPressed, this.radius = 20.0, @required this.text, this.height = 40,
    this.fontSize = 16.0, this.enable, this.background = AppColors.primaryColor});

  final GestureTapCallback onPressed;
  final double radius, height, fontSize;
  final String text;
  final bool enable;
  final Color background;

  @override
  Widget build(BuildContext context) {

    return SizedBox(
      height: height,
      width: double.infinity,
      child: RaisedButton(
          disabledColor: AppTheme.getThemeData().accentColor.withOpacity(0.5),
          elevation: enable ? 6.0 : 0,
          color: enable ?
          AppTheme.getThemeData().accentColor :
          AppTheme.getThemeData().accentColor.withOpacity(0.7),
          child: Text(text,
            style: TextStyle(color:enable ?  background
                : background.withOpacity(0.7),
                fontSize: fontSize),
          ),
          onPressed: enable ? onPressed : (){},
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(radius)
          )
      ),
    );
  }
}
