import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DialogMessage extends StatelessWidget {
  DialogMessage({this.id, this.checkCode});

  final String id, checkCode;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Registro correcto"),

      content:Container(
        height: 100,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("Número de registro:"),
            Text(id, style: TextStyle(fontWeight: FontWeight.w700),),
            Text("Clave:"),
            Text(checkCode, style: TextStyle(fontWeight: FontWeight.w700),)
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
            child: Text("Aceptar"),
            textColor: Colors.blue,
            onPressed: () {
              Navigator.of(context).pop();
            }),
      ],
    );
  }
}
