import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Alert {
  static final int lengthShort = 1; //1 seconds
  static final int lengthLong = 2; // 2 seconds
  static final int lengthVeryLong = 3; // 3 seconds

  static final int top = 1;
  static final int bottom = 2;

  static void show(BuildContext context,
      {String message,
        int duration = 3,
        Color background = Colors.red,
        Color textColor = Colors.white,
        Color iconColor = Colors.white,
        double sizeFont = 12.0,
        IconData icon = Icons.close}) {
    OverlayView.createView(context,
        message: message,
        duration: duration,
        background: background,
        textColor: textColor,
        iconColor: iconColor,
        sizeFont: sizeFont,
        icon: icon);
  }
}

class OverlayView {
  static final OverlayView _singleton = new OverlayView._internal();

  factory OverlayView() {
    return _singleton;
  }

  OverlayView._internal();

  static OverlayState _overlayState;
  static OverlayEntry _overlayEntry;
  static bool _isVisible = false;

  static void createView(BuildContext context,
      {String message,
        int duration,
        Color background,
        Color textColor,
        Color iconColor,
        double sizeFont,
        IconData icon}) {
    _overlayState = Navigator.of(context).overlay;

    if (!_isVisible) {
      _isVisible = true;

      _overlayEntry = OverlayEntry(builder: (context) {
        return EdgeOverlay(
          message: message,
          background: background,
          overlayDuration: duration == null ? Alert.lengthShort : duration,
          textColor: textColor,
          iconColor: iconColor,
          sizeFont: sizeFont,
          icon: icon == null ? Icons.notifications : icon,
        );
      });

      _overlayState.insert(_overlayEntry);
    }
  }

  static dismiss() async {
    if (!_isVisible) {
      return;
    }
    _isVisible = false;
    _overlayEntry?.remove();
  }
}

class EdgeOverlay extends StatefulWidget {
  final String message;
  final Color background;
  final int overlayDuration;
  final Color textColor;
  final Color iconColor;
  final double sizeFont;
  final IconData icon;

  EdgeOverlay(
      {this.message,
        this.background,
        this.overlayDuration,
        this.textColor,
        this.iconColor,
        this.sizeFont,
        this.icon});

  @override
  _EdgeOverlayState createState() => _EdgeOverlayState();
}

class _EdgeOverlayState extends State<EdgeOverlay>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Tween<Offset> _positionTween;
  Animation<Offset> _positionAnimation;

  @override
  void initState() {
    super.initState();

    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 750));
    _positionTween = Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset.zero);
    _positionAnimation = _positionTween.animate(
        CurvedAnimation(parent: _controller, curve: Curves.fastOutSlowIn));
    _controller.forward();

    listenToAnimation();
  }

  listenToAnimation() async {
    _controller.addStatusListener((listener) async {
      if (listener == AnimationStatus.completed) {
        await Future.delayed(Duration(seconds: widget.overlayDuration));
        _controller.reverse();
        await Future.delayed(Duration(milliseconds: 700));
        OverlayView.dismiss();
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;

    return Positioned(
      top: statusBarHeight,
      child: SlideTransition(
        position: _positionAnimation,
        child: Container(
          width: MediaQuery.of(context).size.width,
          color: widget.background,
          child: OverlayWidget(
            message: widget.message,
            textColor: widget.textColor,
            sizeFont: widget.sizeFont,
            icon: widget.icon,
            iconColor: widget.iconColor,
          ),
        ),
      ),
    );
  }
}

class OverlayWidget extends StatelessWidget {
  final String message;
  final Color background;
  final Color textColor;
  final Color iconColor;
  final double sizeFont;
  final IconData icon;

  OverlayWidget({
    @required this.message,
    @required this.textColor,
    @required this.iconColor,
    @required this.sizeFont,
    @required this.icon,
    this.background,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Material(
        type: MaterialType.transparency,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(left: 22.0, bottom: 22.0),
                child: Text(
                  message,
                  style: TextStyle(
                      color: textColor, fontSize: sizeFont),
                ),
              ),
            ),
            Container(
                margin: const EdgeInsets.only(right: 22.0, bottom: 22.0),
                child: InkWell(
                  onTap: () {
                    OverlayView.dismiss();
                  },
                  child: Icon(icon, color: iconColor),
                ))
          ],
        ),
      ),
    );
  }
}
