import 'package:checker/common/DialogMessage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Future<void> showDialogMessage({
  String id,
  String checkCode,
  BuildContext context,
}) async {
  await showDialog(
      context: context,
      builder: (_) => DialogMessage(
        id: id,
        checkCode: checkCode,
      ));
}

String validateOnlyLetters(String value) {
  Pattern pattern = r'^[A-Z a-z áéíóúÁÉÍÓÚñÑ\s]+$';
  RegExp regex = new RegExp(pattern);
  if (!regex.hasMatch(value))
    return 'Campo incorrecto';
  else
    return null;
}

String validateOnlyNumber(String value) {
  Pattern pattern = r'^\d+$';
  RegExp regex = new RegExp(pattern);
  if (!regex.hasMatch(value))
    return 'Campo incorrecto';
  else
    return null;
}


