import 'package:checker/core/Http.dart';
import 'package:checker/core/models/ApiOptions.dart';
import 'package:dio/dio.dart';

String getPath(String path, [String url]) {
  return "http://192.168.0.26:3000/$path";
}

class ApiService {
  static Future<Response> get(String path, [ApiOptions opts]) async {
    return Http().get<Map<String, dynamic>>(
        getPath(path),
        options: opts,
        queryParameters: opts != null ? opts.queryParams : null,
        cancelToken: opts != null ? opts.cancelToken : null,
        onReceiveProgress: opts != null ? opts.onReceiveProgress : null,
      );
  }

  static Future<Response> getList(String path, [ApiOptions opts]) async {
    return Http().get<List<dynamic>>(
      getPath(path),
      options: opts,
      queryParameters: opts != null ? opts.queryParams : null,
      cancelToken: opts != null ? opts.cancelToken : null,
      onReceiveProgress: opts != null ? opts.onReceiveProgress : null,
    );
  }

  static Future<Response> postList(String path, Map body,
      [ApiOptions opts]) async {
    return Http().post<List<dynamic>>(
      getPath(path),
      data: body,
      options: opts,
      cancelToken: opts != null ? opts.cancelToken : null,
      onReceiveProgress: opts != null ? opts.onReceiveProgress : null,
    );
  }

  static Future<Response> post(String path, Map body,
      [ApiOptions opts]) async {
    return Http().post<Map<String, dynamic>>(
        getPath(path),
        data: body,
        options: opts,
        cancelToken: opts != null ? opts.cancelToken : null,
        onReceiveProgress: opts != null ? opts.onReceiveProgress : null,
      );
  }

  static Future<Response> put(String path, Map body,
      [ApiOptions opts]) async {
    return Http().put<Map<String, dynamic>>(
        getPath(path),
        data: body,
        options: opts,
        queryParameters: opts != null ? opts.queryParams : null,
        cancelToken: opts != null ? opts.cancelToken : null,
        onReceiveProgress: opts != null ? opts.onReceiveProgress : null,
      );
  }

  static Future<Response> delete(String path, [ApiOptions opts]) async {
    return Http().delete<Map<String, dynamic>>(
        getPath(path),
        options: opts,
        queryParameters: opts != null ? opts.queryParams : null,
        cancelToken: opts != null ? opts.cancelToken : null,
      );
  }
}
