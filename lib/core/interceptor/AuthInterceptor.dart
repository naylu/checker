import 'package:checker/modules/admin/data/AdminRepository.dart';
import 'package:dio/dio.dart';

class AuthInterceptor extends Interceptor {
  @override
  Future onRequest(RequestOptions options) async {
    String token = await AdminRepository().token;

    if (token != null) {
      token= "Bearer "+token;
      options.headers.putIfAbsent('Authorization', () => token);
    }
    return super.onRequest(options);
  }

  @override
  Future onResponse(Response response) async {
    return super.onResponse(response);
  }

  @override
  Future onError(DioError err) async {
    return super.onError(err);
  }
}
