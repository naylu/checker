import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class PrintInterceptor extends PrettyDioLogger {
  PrintInterceptor(
      {bool request = true,
        bool requestHeader = false,
        bool requestBody = false,
        bool responseHeader = false,
        bool responseBody = true,
        bool error = true,
        int maxWidth = 90,
        bool compact = true,
        Function(Object object) logPrint = print})
      : super(
    request: request,
    requestHeader: requestHeader,
    requestBody: requestBody,
    responseHeader: responseHeader,
    responseBody: responseBody,
    error: error,
    maxWidth: maxWidth,
    compact: compact,
    logPrint: logPrint,
  );

  @override
  Future onRequest(RequestOptions options) async {
    if (_filter(options)) {
      return options;
    }
    return super.onRequest(options);
  }

  @override
  Future onError(DioError err) async {
    final options = err.request;
    if (_filter(options)) {
      return err;
    }
    return super.onError(err);
  }

  @override
  Future onResponse(Response response) async {
    final options = response.request;
    if (_filter(options)) {
      return response;
    }
    return super.onResponse(response);
  }

  bool _filter(RequestOptions options) {
    return false;
  }
}
