import 'package:checker/core/interceptor/AuthInterceptor.dart';
import 'package:checker/core/interceptor/PrintInterceptor.dart';
import 'package:checker/core/models/ApiInvalidResponse.dart';
import 'package:checker/core/models/ApiResponse.dart';
import 'package:dio/dio.dart';
import 'package:dio/native_imp.dart';

class _ContentTypeInterceptor extends Interceptor {
  @override
  Future onRequest(RequestOptions options) {
    options.headers.addAll({
      'Content-Type': 'application/json; charset=utf-8',
    });
    return super.onRequest(options);
  }
}

class _ApiResponseInterceptor extends Interceptor {
  final Dio dio;
  _ApiResponseInterceptor(this.dio);

  @override
  Future onResponse(Response response) {
    var data = response.data;
    if (data != null && data is Map) {
      if (data['status'] == ApiResponseStatus.error.toString()) {
        return dio.reject(ApiInvalidResponse.fromJson(data));
      }
    }
    return super.onResponse(response);
  }
}

final _initialOptions = BaseOptions();

class Http extends DioForNative {
  static final Http _singleton = new Http._internal();

  factory Http() {
    return _singleton;
  }

  Http._internal() : super(_initialOptions) {
    interceptors.add(_ContentTypeInterceptor());

    interceptors.add(_ApiResponseInterceptor(this));

    interceptors.add(AuthInterceptor());

    interceptors.add(
      PrintInterceptor(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        compact: false,
      ),
    );
  }
}
