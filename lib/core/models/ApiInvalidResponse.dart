import 'package:checker/core/models/ApiResponse.dart';

class ApiInvalidResponse extends ApiResponse {
  ApiInvalidResponse({
    String message,
    dynamic data,
  }) : super(
    status: ApiResponseStatus.error,
    message: message,
    data: data,
  );

  factory ApiInvalidResponse.fromJson(Map<String, dynamic> json) =>
      ApiInvalidResponse(
        message: json["message"],
        data: json["data"],
      );
}
