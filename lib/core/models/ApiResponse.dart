enum ApiResponseStatus { success, error }

class ApiResponse {
  final ApiResponseStatus status;
  final String message;
  dynamic data;
  String refreshToken;

  ApiResponse({
    this.status,
    this.message,
    this.data,
    this.refreshToken,
  });

  factory ApiResponse.fromJson(Map<String, dynamic> json) => ApiResponse(
    status: ApiResponseStatus.values.firstWhere(
            (e) => e.toString() == "ApiResponseStatus.${json["status"]}"),
    message: json["message"],
    data: json["data"],
    refreshToken: json["refreshToken"],
  );

  Map<String, dynamic> toJson() => {
    "status": status.toString().replaceAll("ApiResponseStatus.", ""),
    "message": message,
    "data": data,
    "refreshToken": refreshToken,
  };
}
