import 'package:checker/core/models/ApiResponse.dart';

class ApiValidResponse extends ApiResponse {
  ApiValidResponse({
    String message,
    dynamic data,
  }) : super(
    status: ApiResponseStatus.success,
    message: message,
    data: data,
  );

  factory ApiValidResponse.fromJson(Map<String, dynamic> json) =>
      ApiValidResponse(
        message: json["message"],
        data: json["data"],
      );
}

