import 'dart:io';

import 'package:dio/dio.dart';

class ApiOptions extends RequestOptions {
  final Map<String, dynamic> queryParams;
  final CancelToken cancelToken;
  final Function(int, int) onReceiveProgress;
  ApiOptions({
    String method,
    int connectTimeout,
    int sendTimeout,
    int receiveTimeout,
    Iterable<Cookie> cookies,
    Map<String, dynamic> extra,
    Map<String, dynamic> headers,
    ResponseType responseType,
    ContentType contentType,
    ValidateStatus validateStatus,
    bool receiveDataWhenStatusError,
    bool followRedirects,
    int maxRedirects,
    this.queryParams,
    this.cancelToken,
    this.onReceiveProgress,
  }) : super(
    method: method,
    sendTimeout: sendTimeout,
    receiveTimeout: receiveTimeout,
    extra: extra,
    headers: headers,
    responseType: responseType,
    validateStatus: validateStatus,
    receiveDataWhenStatusError: receiveDataWhenStatusError,
    followRedirects: followRedirects,
    maxRedirects: maxRedirects,
  );

  copyWith(
      {String method,
        int connectTimeout,
        int sendTimeout,
        int receiveTimeout,
        Iterable<Cookie> cookies,
        Map<String, dynamic> extra,
        Map<String, dynamic> headers,
        ResponseType responseType,
        ContentType contentType,
        ValidateStatus validateStatus,
        bool receiveDataWhenStatusError,
        bool followRedirects,
        int maxRedirects,
        Map<String, dynamic> queryParams,
        CancelToken cancelToken,
        Function(int, int) onReceiveProgress}) {
    return ApiOptions(
      method: method ?? this.method,
      sendTimeout: sendTimeout ?? this.sendTimeout,
      receiveTimeout: receiveTimeout ?? this.receiveTimeout,
      extra: extra ?? this.extra,
      headers: headers ?? this.headers,
      responseType: responseType ?? this.responseType,
      validateStatus: validateStatus ?? this.validateStatus,
      receiveDataWhenStatusError:
      receiveDataWhenStatusError ?? this.receiveDataWhenStatusError,
      followRedirects: followRedirects ?? this.followRedirects,
      maxRedirects: maxRedirects ?? this.maxRedirects,
      queryParams: queryParams ?? this.queryParams,
      cancelToken: cancelToken ?? this.cancelToken,
      onReceiveProgress: onReceiveProgress ?? this.onReceiveProgress,
    );
  }
}
